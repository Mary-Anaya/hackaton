import "./App.css";
import { Home } from "./pages/Home";
import { Routes, Route, Link } from "react-router-dom";
import { ComponenteFinal } from "./components/ComponenteListado";
import { NavBar } from "../src/components/navbar/NavBar";
import {Footer} from '../src/components/footer/Footer'
import {TextArea} from '../src/pages/TextArea'

function App() {  
  return (
    <div className="App" >
      <NavBar />
      <div>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/generator" element={<ComponenteFinal />} />         
        </Routes>
      </div>
      <Footer />
    </div>
  );
}

export default App;
