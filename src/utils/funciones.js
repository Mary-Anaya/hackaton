export function hacerGrupos(datos, num_integrantes) {
  let arreglo_equipos = [];
  while (datos.length > 0) {
    let arreglo = [];
    while (arreglo.length < num_integrantes) {
      let aleatorio = Math.floor(Math.random() * datos.length);
      arreglo.push(datos[aleatorio]);
      datos.splice(aleatorio, 1);
      if (datos.length === 0) {
        break;
      }
    }
    arreglo_equipos.push(arreglo);
    arreglo = [];
  }
  return arreglo_equipos;
}