import { NumeroIntegrantes } from "./NumeroIntegrantes";
import { ObtenerArchivo } from "./ObtenerArchivo";
import { Tarjeta } from "./Tarjeta";
import { useState, useEffect } from "react";

localStorage.setItem("arreglo", ""); 
export function ComponenteFinal(){
  const [valor, setValor] = useState(1);
  let [data, setData] = useState([]);
    return (
      <div className="App  py-4" style={{ width: "89%", marginLeft: "10%" }}>
        <div className="row pt-3">
          <div className="col-4"></div>
          <div className="col-4">
            <NumeroIntegrantes
              valor={valor}
              setValor={setValor}
              setData={setData}
            />
          </div>
          <div className="col-4">
            <ObtenerArchivo setData={setData} cantidad_participantes={valor} />
          </div>
        </div>

        <div className="row">
          <h1>Grupos Generados</h1>
          {data.map((grupo) => (
            <Tarjeta datos={grupo} />
          ))}
        </div>
      </div>
    );
}